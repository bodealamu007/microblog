from app import app # from app module import app package
from flask import render_template

@app.route('/')
@app.route('/index')
def index():
    user = {'username':'Olabode'}

    posts = [
        {
            'author': {'username': 'John'},
            'body': 'Beautiful day in Portland!'
        },
        {
            'author': {'username': 'Susan'},
            'body': 'The Avengers movie was so cool!'
        }
    ]
    return render_template('index.html',title='Home',user=user,posts=posts)