from flask import Flask
from config import Config
app = Flask(__name__)

# tell flask where to get the config file from
app.config.from_object(Config)

# imports the route module from the app package
from app import routes